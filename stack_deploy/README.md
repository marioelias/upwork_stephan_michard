## Step by step to deploy the stack
### This was tested on a GKE with Kubernetes 1.19+
### 1. Deploy CRDS for cert-manager
```
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.3.1/cert-manager.crds.yaml
```
### 2. Deploy cert-manager
```
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.3.1
```
#### Validate that all pods are up and running
```
kubectl get pods --namespace cert-manager
```
### 3. Deploy Cluster Issuers
```
kubectl apply -f cert_manager_ops/cluster_issuer_staging.yml
kubectl apply -f cert_manager_ops/cluster_issuer_production.yml
```
### 4. Deploy Nginx Ingress Controller

#### Before installing
 - Line 208 of nginx_ingres/values.yaml you can modify the IP address that you want to use for the loadbalancer, if leave it empty it will be auto assign by GKE.

```
helm repo add nginx-stable https://helm.nginx.com/stable
helm install nginx-ing nginx-stable/nginx-ingress --create-namespace --namespace nginx-controller -f nginx_ingress/values.yaml
```
#### Validate that all pods are up and running
```
kubectl get pods -n nginx-controller
```

### 5. Deploy Concourse
```
helm repo add concourse https://concourse-charts.storage.googleapis.com/
helm install concourse concourse/concourse --create-namespace --namespace concourse -f concourse_ops/values.yaml
```
#### Validate that ingress is created
```
kubectl get ing -n concourse
kubectl describe ing [ingress-name] -n concourse
```

### 6. Deploy App
```
kubectl create ns demo-app
kubectl apply -f demo_app_1/deployment_1.yml -n demo-app
kubectl apply -f demo_app_1/service_1.yml -n demo-app
kubectl apply -f demo_app_1/ingress_1.yml -n demo-app
```
## Deploy Contour with helm
```
helm repo add bitnami https://charts.bitnami.com/bitnami
kubectl create ns projectcontour
helm install contour bitnami/contour -n projectcontour -f values.yaml
```

# Notes
The values file already have setup the IP address from your notes in the line
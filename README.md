# Notes

## Deploy Contour and Cert-Manager
[Link to Docs](https://projectcontour.io/guides/cert-manager/)
 
### Deploy Contour
```
kubectl apply -f 01_contour/deploy_contour.yml  
kubectl get -n projectcontour service envoy -o wide
```

Patch IP of external Loadbalancer with static IP
```
export CLOUD_STATIC_IP=34.89.167.234
kubectl patch service envoy --namespace projectcontour --patch "{\"spec\": { \"loadBalancerIP\": \"$CLOUD_STATIC_IP\" }}"
```

### Deploy Cert-Manager + Cluster Issuer
```
kubectl apply -f 01_contour/deploy_cert_manager.yml
kubectl apply -f 01_contour/cluster_issuer_production.yml  
```

### Deploy App: Deployment, Service and Ingress
```
kubectl apply -f demo_app_1/deployment_1.yml
kubectl apply -f demo_app_1/service_1.yml
kubectl apply -f demo_app_1/ingress_1.yml
```


## Concourse CI
[Link to Docs](https://github.com/concourse/concourse-chart)
```
helm repo add concourse https://concourse-charts.storage.googleapis.com/
helm install my-release concourse/concourse -f values.yml
kubectl apply -f concourse_ingress.yml
helm delete my-release
```


## Deploy CertManager with Helm

https://medium.com/avmconsulting-blog/encrypting-the-certificate-for-kubernetes-lets-encrypt-805d2bf88b2a


## Installing Apps

1. Installing contour
## Deploy Contour with helm
https://github.com/bitnami/charts/tree/master/bitnami/contour
```
helm repo add bitnami https://charts.bitnami.com/bitnami
kubectl create ns projectcontour
helm install contour bitnami/contour -n projectcontour -f contour_ops/values.yaml
```

# Notes
The values file already have setup the IP address from your notes in the line

2. Installing cert manager

Before installing the chart, you must first install the cert-manager CustomResourceDefinition resources. This is performed in a separate step to allow you to easily uninstall and reinstall cert-manager without deleting your installed custom resources.
```
$ kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.3.1/cert-manager.crds.yaml
```
To install the chart with the release name my-release:

## Add the Jetstack Helm repository
```
$ helm repo add jetstack https://charts.jetstack.io
```
## Install the cert-manager helm chart
```
kubectl create ns cert-manager
helm install cert-manager --namespace cert-manager jetstack/cert-manager
kubectl apply -f 01_contour/cluster_issuer_staging.yml
kubectl apply -f 01_contour/cluster_issuer_production.yml
```
In order to begin issuing certificates, you will need to set up a ClusterIssuer or Issuer resource (for example, by creating a 'letsencrypt-staging' issuer).

3. Installing Concourse
helm repo add concourse https://concourse-charts.storage.googleapis.com/
kubectl create ns concourse
helm install concourse concourse/concourse -n concourse -f concourse_ops/values.yaml


helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.3.1 \
  --set installCRDs=true
## Deploy GKE Service
https://estl.tech/configuring-https-to-a-web-service-on-google-kubernetes-engine-2d71849520d
https://cloud.google.com/kubernetes-engine/docs/tutorials/http-balancer



## Nginx Ingress
kubectl create ns nginx-controller
helm repo add nginx-stable https://helm.nginx.com/stable
helm install nginx-ing nginx-stable/nginx-ingress -n nginx-controller -f nginx_ingress/values.yaml




https://github.com/jetstack/cert-manager/issues/2408